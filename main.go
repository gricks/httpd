package main

import (
	"flag"
	"net/http"

	"github.com/rs/cors"
)

var (
	addr = flag.String("addr", ":80", "Listen Address")
	path = flag.String("path", ".", "Server Path")
)

func init() {
	flag.Parse()
}

func main() {
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir(*path)))
	if err := http.ListenAndServe(*addr, cors.Default().Handler(mux)); err != nil {
		panic(err)
	}
}
